'use strict';

pbApp.controller('pbMainController', function($scope)
{
	$scope.header 		= 'Contact List';
	$scope.regex_mobile = /^\(?(\d{4})\)?[-]?(\d{7})$/;
	$scope.contacts 	= [];

    $scope.contacts.push({"name": "Nava, Danilo Jr.", "contact_number": "09433085773"});
    $scope.contacts.push({"name": "Abao, Emmanuel", "contact_number": "09175458527"});

    console.log($scope.contacts);

    $scope.info = function(index, contact)
    {
    	jQuery('#modal-info .modal-title').html('Contact Details');
    	jQuery('#modal-info .contact-name').val(contact.name);
    	jQuery('#modal-info .contact-number').val(contact.contact_number);

    	jQuery('#modal-info').modal('show');
    	console.log('INFO', contact);
    }

    $scope.new = function()
    {
    	jQuery('#modal-newedit .modal-title').html('New Contact');
    	jQuery('#modal-newedit').attr('data-index', -1);
    	jQuery('#modal-newedit .contact-name').val('');
    	jQuery('#modal-newedit .contact-number').val('');

    	jQuery('#modal-newedit').modal('show');
    	console.log('NEW');

    	$scope.formNewEdit.contactName.$setViewValue("");
    	$scope.formNewEdit.contactNumber.$setViewValue("");
    	$scope.formNewEdit.$setPristine();
    	console.log($scope.formNewEdit);
    }

    $scope.edit = function(index, contact)
    {
    	jQuery('#modal-newedit .modal-title').html('Update Contact');
    	jQuery('#modal-newedit').attr('data-index', index);
    	jQuery('#modal-newedit .contact-name').val(contact.name);
    	jQuery('#modal-newedit .contact-number').val(contact.contact_number);

    	// use to reset the angular validation..
    	//console.log($scope.formNewEdit);
    	$scope.formNewEdit.contactName.$setViewValue(contact.name);
    	$scope.formNewEdit.contactNumber.$setViewValue(contact.contact_number);

    	jQuery('#modal-newedit').modal('show');
    	console.log('EDIT', contact);
    }

    $scope.delete = function(index, contact)
    {
    	$scope.contacts.splice(index, 1);
    	console.log('DELETE', contact);
    }

    $scope.save = function(contact_form)
    {
    	console.log(contact_form);

    	if (contact_form.$valid)
    	{
    		var contact_index 	= jQuery('#modal-newedit').attr('data-index');
	    	var contact_name 	= jQuery('#modal-newedit .contact-name').val();
	    	var contact_number 	= jQuery('#modal-newedit .contact-number').val();

	    	// new..
	    	if (contact_index == -1)
	    	{
	    		$scope.contacts.push({'name': contact_name, 'contact_number': contact_number});
	    	}
	    	else // edit..
	    	{
	    		$scope.contacts[ contact_index ].name = contact_name;
	    		$scope.contacts[ contact_index ].contact_number = contact_number;

	    		console.log('UPDATED..');

	    		/*angular.forEach($scope.contacts, function(contact, index)
	    		{
	    			console.log(index, contact);
	    			console.log(contact.id, contact_id);

					if ($scope.contacts[ index ].id == contact_id)
					{
						contact.name = contact_name;
						contact.contact_number = contact_number;

						console.log('UPDATED..');
					}
				});*/
	    	}

	    	jQuery('#modal-newedit').modal('hide');
	    	console.log('SAVE', $scope.contacts);
    	}
    }
});