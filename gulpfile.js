var gulp 			= require('gulp');
var stylus 			= require('gulp-stylus');
var gutil 			= require('gulp-util');
var concat 			= require('gulp-concat');
var uglify 			= require('gulp-uglify');
var imagemin 		= require('gulp-imagemin');
var sourcemaps 		= require('gulp-sourcemaps');
var del 			= require('del');

gulp.task('default', function()
{
	return gutil.log('Gulp is running!');
});

// Not all tasks need to use streams 
// A gulpfile is just another node program and you can use all packages available on npm 
gulp.task('clean', function(cb)
{
	del(['build'], cb);
});
 
gulp.task('scripts', ['clean'], function()
{
	// Minify and copy all JavaScript (except vendor scripts) 
	// with sourcemaps all the way down 
	return gulp.src(paths.scripts)
		.pipe(sourcemaps.init())
			.pipe(coffee())
			.pipe(uglify())
			.pipe(concat('all.min.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/build/js'));
});

// Copy all static images 
gulp.task('images', ['clean'], function()
{
	return gulp.src(paths.images)
		.pipe(imagemin({optimizationLevel: 5}))
		.pipe(gulp.dest('./public/build/img'));
});

gulp.task('stylus', function ()
{
	gulp.src('./public/css/stylus/*.styl')
		.pipe(stylus({style: "compressed"}))
		.pipe(gulp.dest('./public/build/css'));
});

gulp.task('serve', function()
{
	var express        	= require('express');
	var expressWinston 	= require('express-winston');
	var winston 		= require('winston');
	var app            	= express();
	var port 			= 8080;

	// express-winston logger makes sense BEFORE the router.
	app.use(expressWinston.logger({transports: [new winston.transports.Console({json: true, colorize: true})]}));

	app.set('views', __dirname + '/views')
	app.set('view engine', 'jade')
	app.use(express.static(__dirname + '/public'));

	require('./app/routes')(app);

	// express-winston errorLogger makes sense AFTER the router.
	app.use(expressWinston.errorLogger({transports: [new winston.transports.Console({json: true, colorize: true})]}));
	app.listen(port, function (argument) { console.log('Listening on port ' + port); });

	exports = module.exports = app;
});

gulp.task('default', ['scripts', 'stylus', 'serve']);