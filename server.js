var express        	= require('express');
var expressWinston 	= require('express-winston');
var winston 		= require('winston');
var app            	= express();
var port 			= 8080;

// express-winston logger makes sense BEFORE the router.
app.use(expressWinston.logger({transports: [new winston.transports.Console({json: true, colorize: true})]}));

app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.static(__dirname + '/public'));

require('./app/routes')(app);

// express-winston errorLogger makes sense AFTER the router.
app.use(expressWinston.errorLogger({transports: [new winston.transports.Console({json: true, colorize: true})]}));
app.listen(port, function (argument) { console.log('Listening on port ' + port); });

exports = module.exports = app;